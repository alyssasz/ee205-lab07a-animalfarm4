///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file testing.cpp
/// @version 1.0
///
/// Test the random generators 
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   Mar 13 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <unistd.h>

#include "animal.hpp"
#include "animalFactory.hpp"

using namespace std;
using namespace animalfarm;

int main () {

    cout << "Testing Random Generator" << endl;

    for (int i = 0; i < 10; i++) {
        //cout << Animal::getRandomGender() << endl; 
        //cout << Animal::getRandomColor()  << endl;
        //cout << Animal::getRandomBool()   << endl;
        //cout << Animal::getRandomWeight(3.0,8.5) << endl;
        //cout << Animal::getRandomName()   << endl;
        Animal* a = AnimalFactory::getRandomAnimal();
        cout << a->speak() << endl; 
        //sleep(1); 
    }



}




