///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   Feb 17 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {
	
Nene::Nene(string newTagID, enum Color newColor, enum Gender newGender ) {
	tagID = newTagID;					/// A has-a relationship, not all nenes are founded 
	species = "Branta sandvicensis";	/// Hardcode, is-a relationship, all nenes are the same species
	gender = newGender;					/// A has-a relationship, not all nenes are the same gender
	featherColor = newColor;			/// A has-a relationship, not all nenes have the same feather color
	isMigratory = true;					/// Hardcode, is-a relationship, nenes have either migrated or not
}

/// The nene says something different compared to other birds
const string Nene::speak() {
	return string( "Nay, nay" );
}

/// Prints Nene first, the tag ID, whatever information Bird holds
void Nene::printInfo() {
	cout << "Nene" << endl;
	cout << "   Tag ID = [" << tagID << "]" << endl;
	
	Bird::printInfo();
}

} // namespace animalfarm