###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author Alyssa Zhang <alyssasz@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   Feb 17 2021
###############################################################################

all: main

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
animalFactory.o: animalFactory.hpp animalFactory.cpp
	g++ -c animalFactory.cpp

mammal.o: mammal.hpp mammal.cpp animal.hpp
	g++ -c mammal.cpp

fish.o: fish.hpp fish.cpp animal.hpp
	g++ -c fish.cpp

bird.o: bird.hpp bird.cpp animal.hpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp mammal.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp mammal.hpp
	g++ -c dog.cpp

nunu.o: nunu.hpp nunu.cpp fish.hpp
	g++ -c nunu.cpp

aku.o: aku.hpp aku.cpp fish.hpp
	g++ -c aku.cpp

palila.o: palila.hpp palila.cpp bird.hpp
	g++ -c palila.cpp

nene.o: nene.hpp nene.cpp bird.hpp
	g++ -c nene.cpp

node.o: node.hpp node.cpp 
	g++ -c node.cpp	

list.o: list.hpp list.cpp 
	g++ -c list.cpp	

main: main.cpp *.hpp main.o animal.o animalFactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o 
	g++ -o main main.o animal.o animalFactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o
	
clean:
	rm -f *.o main
