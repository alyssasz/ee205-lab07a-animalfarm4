///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   Feb 17 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {
	
Aku::Aku(float newWeight, enum Color newColor, enum Gender newGender ) {
	weight = newWeight;					/// A has-a relationship, not all akus have the same weight 
	species = "Katsuwonus pelamis";		/// Hardcode, is-a relationship, all akus are the same species
	gender = newGender;					/// A has-a relationship, not all akus are the same gender
	scaleColor = newColor;				/// A has-a relationship, not all akus have the same scale color
	favoriteTemp = 75;					/// Hardcode, is-a relationship, all akus have the same fav temperature
}

/// Prints Aku first, it's weight, whatever information Fish holds
void Aku::printInfo() {
	cout << "Aku" << endl;
	cout << "   Weight = [" << weight << "]" << endl;
	
	Fish::printInfo();
}

} // namespace animalfarm