///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   Feb 17 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm {
	  
// constructor
Animal::Animal() {
   cout << ".";
    
}

// destructor
Animal::~Animal() {
   cout << "x";
}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
   switch (color) {
      
      case BLACK:    return string("Black"); break; 
      case WHITE:    return string("White"); break;
      case RED:      return string("Red"); break; 
      case SILVER:   return string("Silver"); break; 
      case YELLOW:   return string("Yellow"); break; 
      case BROWN:    return string("Brown"); break;  

   }

   return string("Unknown");
};
	
const enum Gender Animal::getRandomGender() {

   int randomGender = rand() % 2;

   switch(randomGender) {
      
      case 0: return MALE; break;
      case 1: return FEMALE; break;

   }

   return MALE;

} 

const enum Color Animal::getRandomColor() {

   int randomColor = rand() % 6; 

   switch(randomColor) {
      
      case 0: return BLACK; break;
      case 1: return WHITE; break;
      case 2: return RED; break;
      case 3: return SILVER; break;
      case 4: return YELLOW; break;
      case 5: return BROWN; break;

   }

   return BLACK; 

}
 
const bool Animal::getRandomBool() {

   int randomBool = rand() % 2;

   switch(randomBool) {

      case 0: return false; break;
      case 1: return true; break;

   }

   return true; 

}

const float Animal::getRandomWeight(const float from, const float to) {

   int randomNumber = (rand() % int(to - from));
   float randomWeight = from + randomNumber; //so it's at the min length 

   return randomWeight; 
}

const string Animal::getRandomName() {
   
   //srand() is used to make it a different random set each compilation with sleep(), to test if the random generator works
   //srand(time(NULL)); 
  
   int min = 4;
   int max = 9;

   int randomLength = (rand() % (max - min + 1)) + min; // so the randomName fits within the parameters

   char randomUpper = 'A' + rand() % 26; //from the ASCII table, it will generate a random number assoicated with a char   

   string randomName;
   randomName = randomName + randomUpper;

   for (int i = 1; i < randomLength; i++) {
      char randomLower = 'a' + rand() % 26; //from the ASCII table, it will generate a random number assoicated with a char
      
      randomName = randomName + randomLower;
   }  
   return randomName;

}


} // namespace animalfarm
