///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalFactory.cpp
/// @version 1.0
///
/// Returns a random Animal
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   Mar 13 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "animalFactory.hpp"
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;


namespace animalfarm {

Animal* AnimalFactory::getRandomAnimal() {
    Animal* newAnimal = NULL;
    //srand(time(NULL));
    int i = rand() % 6;
    switch(i) {
        case 0: newAnimal = new Cat     (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
        case 1: newAnimal = new Dog     (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
        case 2: newAnimal = new Nunu    (Animal::getRandomBool(), RED, Animal::getRandomGender()); break;
        case 3: newAnimal = new Aku     (Animal::getRandomWeight(3.0, 9.5), SILVER, Animal::getRandomGender()); break;
        case 4: newAnimal = new Palila  (Animal::getRandomName(), YELLOW, Animal::getRandomGender()); break;
        case 5: newAnimal = new Nene    (Animal::getRandomName(), BROWN, Animal::getRandomGender()); break;
  
    } 
    return newAnimal;


};


}//namespace animalfarm





