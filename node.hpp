///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
/// Node class
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   Mar 25 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

namespace animalfarm {

class Node {

friend class SingleLinkedList;

protected: 
    Node* next = nullptr;

};

} // namespace animalfarm






