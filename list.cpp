///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// Singly linked list class
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   Mar 25 2021
///////////////////////////////////////////////////////////////////////////////

#include "list.hpp"

using namespace std;

namespace animalfarm {

const bool SingleLinkedList::empty() const{
    return head == nullptr;
}

void SingleLinkedList::push_front(Node* newNode) {

    if (newNode == nullptr) {
        return;
    }
    
    newNode -> next = head; 
    head = newNode;
    count++;

}

Node* SingleLinkedList::pop_front() {

    if (head == nullptr) {
        return nullptr; 
    }

    Node* temp = head;
    head = head -> next;  
    count--;   
    return temp; 

}

Node* SingleLinkedList::get_first() const {
    return head;
}

Node* SingleLinkedList::get_next(const Node* currentNode) const {
    return currentNode -> next;
}

unsigned int SingleLinkedList::size() const {
    return count;
}

} // namespace animalfarm



