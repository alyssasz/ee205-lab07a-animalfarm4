///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   Feb 17 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {
	
Nunu::Nunu(bool isNative, enum Color newColor, enum Gender newGender ) {
	native = isNative;               	/// A has-a relationship, depending on where the person inputting data is
	species = "Fistularia chinensis";   /// Hardcode, is-a relationship, all nunus are the same species 
	gender = newGender;					/// A has-a relationship, not all nunus are the same gender 
	scaleColor = newColor;              /// A has-a relationship, not all nunus have the same scale color
	favoriteTemp = 80.6;				/// Hardcode, is-a relationship, all nunus have the same fav temperature
}

/// Prints Nunu first, if it's native, whatever information Fish holds
void Nunu::printInfo() {
	cout 				   << "Nunu" << endl;
	cout << std::boolalpha << "   Is native = [" << native << "]" << endl;
	
	Fish::printInfo();
}

} // namespace animalfarm