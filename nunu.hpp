///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.hpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   Feb 17 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "fish.hpp"

using namespace std;


namespace animalfarm {

class Nunu : public Fish {
public:
	bool native;

	Nunu( bool isNative, enum Color newColor, enum Gender newGender );

	void printInfo();
};

} // namespace animalfarm