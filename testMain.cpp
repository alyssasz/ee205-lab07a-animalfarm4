///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file testMain.cpp
/// @version 1.0
///
/// Test Node and Singly Linked List
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   Mar 25 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "node.hpp"
#include "list.hpp"
//#include "animal.hpp"

using namespace std;
using namespace animalfarm;

int main() {

    cout << "Testing Node and Singly Linked List" << endl;
    cout << endl;

    Node node, node1, node2, node3; // Instantiate a node
    SingleLinkedList list; // Instatiate a SingleLinkedList

    cout << std::boolalpha;
    cout << "is it empty: "                        << list.empty() << endl;
    cout << endl;

    list.push_front(&node);
    Node* tempnode1 = list.get_first();
    cout << "this is the first node: "             << tempnode1     << endl;
    cout << "this is the size of the list: "       << list.size()   << endl;
    cout << endl;

    list.push_front(&node1);
    list.push_front(&node2);
    list.push_front(&node3);
    cout << "this is the now the new first node: "    << list.get_first()        << endl;
    cout << "this node is after the current node 2: " << list.get_next(&node2)   << endl;
    cout << "this node is after the current node 1: " << list.get_next(&node1)   << endl;
    cout << "this is the size of the list now: "      << list.size()             << endl;
    cout << endl;

    Node* tempnode2 = list.pop_front();
    cout << "this is the size of the list now: "   << list.size()      << endl;
    cout << "this node is now removed: "           << tempnode2        << endl;
    cout << "is it empty: "                        << list.empty()     << endl;
    cout << endl;

}



